const express = require("express")
const cors = require("cors")
const db = require("./app/models")
const bodyParser = require("body-parser")

const app = express()

const corsOptions = {
    origin : "*"
}

//register cors middleware
app.use(cors(corsOptions))
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Test form
app.use(express.static('test'))

// Konek database
const mongooseConfig = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

db.mongoose.connect(db.url, mongooseConfig)
    .then(() => console.log("Database connected"))
    .catch(err => {
        console.log("Failed to connect to database: ", err.message)
        process.exit()
    })

// Memanggil route mahasiswa
require("./app/routes/routes")(app)

const PORT = process.env.port || 8085
app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
