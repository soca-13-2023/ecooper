import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import NavbarPopup from '@/components/NavbarPopup.vue'

describe('NavbarPopup.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(NavbarPopup, {
      propsData: { msg }
    })
    expect(wrapper.text()).to.include(msg)
  })
})
