module.exports = app => {
    const product = require("../controllers/product.controller");
    const  = require("../controllers/.controller");
    const riwayat = require("../controllers/riwayat.controller");
    const pendapatan = require("../controllers/pendapatan.controller");
    const laporan = require("../controllers/laporan.controller");
    const r = require("express").Router();
    

    r.get("/product", product.findAll);
    r.get("/product/:id", product.show);
    r.post("/product", product.create);
    r.put("/product/:id", product.update);
    r.delete("/product/:id", product.delete);
    r.get("/product/getbycategory/:id", product.findProductByCategory);

    r.get("/", .findAll);
    r.get("//:id", .show);
    r.post("/", .create);
    r.put("//:id", .update);
    r.delete("//:id", .delete);

    r.get("/riwayat", riwayat.findAll);
    r.get("/riwayat/:id", riwayat.show);
    r.post("/riwayat", riwayat.create);
    r.put("/riwayat/:id", riwayat.update);
    r.delete("/riwayat/:id", riwayat.delete);

    r.get("/pendapatan", pendapatan.getTotal);
    r.get("/pendapatan/year/:year", pendapatan.getPerYear);
    r.get("/pendapatan/month/:month", pendapatan.getPerMonth);

    r.get("/laporan", laporan.getLaporan);

    app.use("/", r)
}


