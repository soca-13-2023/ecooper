const Transaksi = global.database.transaksi;
const Utility = require("../helpers/utility.helpers");

/* 
pendapatanPerBulan

{
    "2023": {
        "pendapatan": {
            "kotor": 5000000000,
            "bersih": 423000000
        },
        "pengeluaran": 12300000,
        "bulan": {
            "Februari": {
                "pendapatan": {
                    "kotor": 5000000000,
                    "bersih": 423000000
                },
                "pengeluaran": 12300000,
                "persentaseBarangTerjual": {
                    "25": {
                        "id": 1231232312,
                        "nama": "Topi",
                        "jumlah": 30,
                        "harga": {
                            "jual": 2500,
                            "beli": 2000
                        }
                    },
                    "75": {
                        "id": 231231232,
                        "nama": "Penghapus",
                        "jumlah": 45,
                        "harga": [
                            {
                                "tanggal": 1,
                                "harga": {
                                    "jual": 2000,
                                    "beli": 1500
                                }
                            },
                            {
                                "tanggal": 12,
                                "harga": {
                                    "jual": 2500,
                                    "beli": 1500
                                }
                            }
                        ]
                    }
                },
                "tanggal": {
                    "2": { 
                        "penjualan": {
                            "pendapatan": {
                                "kotor": 50000000,
                                "bersih": 120000
                            },
                            "barang": [
                                {
                                    "id": 1231312323,
                                    "nama": "Spidol",
                                    "jumlah": 10,
                                    "harga": {
                                        "jual": 3000,
                                        "beli": 2500
                                    }
                                },
                                {
                                    "id": null,
                                    "nama": "Print",
                                    "jumlah": 5,
                                    "harga": {
                                        "jual": 500,
                                        "beli": 100
                                    }
                                }
                            ]
                        },
                        "pembelian": {
                            "pengeluaran": 82000,
                            "barang": [
                                {
                                    "nama": "Pensil",
                                    "jumlah": 200,
                                    "harga": {
                                        "jual": 2000,
                                        "beli": 1500
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }
    }
}

*/

const makeLaporan = async (tahun, bulan) => {
    const checkTahun = (tahun) => {
        if (tahun in dataAkhir)
            return;

        dataAkhir["2023"] = {
            "pendapatan": {
                "kotor": 0,
                "bersih": 0
            },
            "pengeluaran": 0,
            "bulan": {}
        }
    }

    const checkBulan = (tahun, bulan) => {
        if (bulan in dataAkhir[tahun].bulan)
            return;

        dataAkhir[tahun].bulan[bulan] = {
            "pendapatan": {
                "kotor": 0,
                "bersih": 0
            },
            "pengeluaran": 0,
            "tanggal": {}
        };
    }

    const checkTanggal = (tahun, bulan, tanggal) => {
        if (tanggal in dataAkhir[tahun].bulan[bulan].tanggal)
            return;

        dataAkhir[tahun].bulan[bulan].tanggal[tanggal] = {
            "penjualan": {
                "pendapatan": {
                    "kotor": 0,
                    "bersih": 0
                },
                "barang": []
            },
            "pembelian": {
                "pengeluaran": 0,
                "barang": []
            }
        };
    }

    let dataAkhir = {};

    // Wait for the query to complete and fetch all documents from the Transaksi collection
    const dataTransaksi = await Transaksi.find();
            
    let pendapatanKotor = 0;
    let pengeluaran = 0;

    for (const transaksi of dataTransaksi) {
        const tahun = transaksi.tanggal.getFullYear();
        const bulan = transaksi.tanggal.getMonth();
        const tanggal = transaksi.tanggal.getDate();

        checkTahun(tahun);
        checkBulan(tahun, bulan);
        checkTanggal(tahun, bulan, tanggal);

        let pendapatanKotor;
        let pendapatanBersih;
        
        if (transaksi.type == "beli")
            pendapatanKotor += transaksi.harga * transaksi.jumlah;
        else if (transaksi.type == "jual")
            pengeluaran += transaksi.harga * transaksi.jumlah;   
    }

    console.log(JSON.stringify(dataAkhir, null, 2));
}

exports.getLaporan = async (req, res) => {
    try {
        makeLaporan();

        // Send the data as a JSON response
        res.json({
            "abc": 123
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ message: err.message });
    }
};
