const db = require("../models");
const Product = db.product;
const Category = db.the-category;

exports.create = (req, res) => {
    Category.create(req.body)
        .then(() => res.send({ message: "Data Berhasil Disimpan" }))
        .catch(err => {
            console.error('Failed when creating Category', err);
            res.status(500).send({ message: err });
        });
};

exports.findAll = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Category collection
        const data = await Category.find();
        // Send the data as a JSON response
        res.json(data);
    } catch (err) {
        // Handle errors
        res.status(500).send({ message: err });
    }
};

exports.show = (req, res) => {
    // Implement logic to find and send a specific Category by ID
    const id = req.params.id;

    Category.findById(id)
    .then(data => res.send(data))
    .catch(err => {
        console.error('Failed when showing Category', err);
        res.status(500).send({ message: err });
    }); 
};

exports.update = (req, res) => {
    // Implement logic to update a Category by ID
    const id = req.params.id;

    Category.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
    .then(data => {
        if (!data) {
            res.status(404).send({message: "Tidak Dapat Mengupdate Data"})
        }
        res.send({message: "Data berhasil di update"})
    })
    .catch(err => {
        console.error('Failed when updating Category', err);
        res.status(500).send({ message: err });
    })
};

exports.delete = (req, res) => {
    // Implement logic to delete a Category by ID
    const id = req.params.id;

    Category.findByIdAndRemove(id)
    .then(data => {
        if (!data) {
            res.status(404).send({message: "Tidak Dapat Menghapus Data"})
        }
        res.send({message: "Data berhasil di Hapus"})
    })
    .catch(err => {
        console.error('Failed when deleting Category', err);
        res.status(500).send({ message: err });
    });
};
