const Riwayat = global.database.riwayat;

exports.getTotal = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find();
        
        let pendapatanKotor = 0;
        let pengeluaran = 0;

        for (const transaksi of data) {
            if (transaksi.type == "jual")
                pendapatanKotor += transaksi.harga * transaksi.jumlah;
            else
                pengeluaran += transaksi.harga * transaksi.jumlah;   
        }

        const pendapatanBersih = pendapatanKotor - pengeluaran;

        // Send the data as a JSON response
        res.json({
            pendapatanBersih: pendapatanBersih,
            pendapatanKotor: pendapatanKotor
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ message: err.message });
    }
};

exports.getPerYear = async (req, res) => {
    const year = req.params.year;

    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find();
        
        let pendapatanKotor = 0;
        let pengeluaran = 0;
        
        for (const transaksi of data) {
            if (transaksi.tanggal.getFullYear() != year)
                continue;

            if (transaksi.type == "jual")
                pendapatanKotor += transaksi.harga * transaksi.jumlah;
            else
                pengeluaran += transaksi.harga * transaksi.jumlah; 
        }

        const pendapatanBersih = pendapatanKotor - pengeluaran;

        // Send the data as a JSON response
        res.json({
            pendapatanBersih: pendapatanBersih,
            pendapatanKotor: pendapatanKotor
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ message: err.message });
    }
};

exports.getPerMonth = async (req, res) => {
    const month = req.params.month;

    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find();
        
        let pendapatanKotor = 0;
        let pengeluaran = 0;
        
        for (const transaksi of data) {
            if (transaksi.tanggal.getMonth() + 1 != month)
                continue;

            if (transaksi.type == "jual")
                pendapatanKotor += transaksi.harga * transaksi.jumlah;
            else
                pengeluaran += transaksi.harga * transaksi.jumlah; 
        }

        const pendapatanBersih = pendapatanKotor - pengeluaran;

        // Send the data as a JSON response
        res.json({
            pendapatanBersih: pendapatanBersih,
            pendapatanKotor: pendapatanKotor
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ message: err.message });
    }
};
