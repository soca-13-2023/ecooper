const Product = global.database.product;
const Category = global.database.the-category;

exports.create = (req, res) => {
    Product.create(req.body)
        .then(() => res.send({ message: "Data Berhasil Disimpan" }))
        .catch(err => {
            console.error('Failed when creating Product', err);
            res.status(500).send({ message: err });
        });
};

exports.findAll = (req, res) => {
    if (req.query.detailed == 'true') {
        this.findAllDetailed(req, res);
    } else {
        this.findAllOptimized(req, res);
    }
};

exports.findAllOptimized = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Product collection
        const rawData = await Product.find()
            .populate('the-category', 'namaCategory')
            .exec();

        const optimizedData = [];

        rawData.forEach(product => {
            if (!product.dijual)
                return;

            optimizedData.push({
                id: product.id,
                namaProduct: product.namaProduct,
                stockProduct: product.stockProduct,
                the-category: product.the-category,
                hargaJual: product.hargaJual
            });
        });

        // Send the data as a JSON response
        res.json(optimizedData);
    } catch (err) {
        console.error('Failed when getting Optimized Product', err);
        res.status(500).send({ message: err });
    }
}

exports.findAllDetailed = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Product collection
        const data = await Product.find()
            .populate('the-category', 'namaCategory')
            .exec();

        // Send the data as a JSON response
        res.json(data);
    } catch (err) {
        console.error('Failed when getting Detailed Product', err);
        res.status(500).send({ message: err });
    }
};

exports.findProductByCategory = async (req, res) => {
    const id = req.params.id;

    const data = await Product.find({ the-category: id })
        .populate('the-category', 'namaCategory')
        .exec();

    console.log(data)
    res.json(data);
}

exports.show = (req, res) => {
    // Implement logic to find and send a specific Product by ID
    const id = req.params.id;

    Product
        .findById(id)
        .populate('the-category', 'namaCategory')
        .then(async data => {
            if (!data) {
                res.status(404).send({message: "Tidak dapat menemukan produk yang dicari"})
                return;
            }

            res.send(data)
        })
        .catch(err => {
            console.error('Failed when showing Product', err);
            res.status(500).send({message: err})
        });
};

exports.update = (req, res) => {
    // Implement logic to update a Product by ID
    const id = req.params.id;

    Product.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
    .then(data => {
        if (!data) {
            res.status(404).send({message: "Tidak Dapat Mengupdate Data"})
        }
        res.send({message: "Data berhasil di update"})
    })
    .catch(err => {
        console.error('Failed when updating Product', err);
        res.status(500).send({ message: err });
    });
};

exports.delete = (req, res) => {
    // Implement logic to delete a Product by ID
    const id = req.params.id;

    Product.findByIdAndRemove(id)
    .then(data => {
        if (!data) {
            res.status(404).send({message: "Tidak Dapat Menghapus Data"})
        }
        res.send({message: "Data berhasil di Hapus"})
    })
    .catch(err => {
        console.error('Failed when deleting Product', err);
        res.status(500).send({ message: err });
    });
};

