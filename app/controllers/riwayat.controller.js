const Riwayat = global.database.riwayat;
const Product = global.database.product;

exports.create = (req, res) => {
    Riwayat.create(req.body)
        .then(() =>  res.send({ message: "Data Berhasil Disimpan" }))
        .catch(err => {
            console.error("Failed creating Transaksi: ", err);
            res.status(404).send({ message: err.message });
        });
};

exports.findAll = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find()
            .populate('product.data', ['_id', 'namaProduct'])
            .exec();

        // Send the data as a JSON response
        res.json(data);
    } catch (err) {
        // Handle errors
        console.error("Failed getting all Transaksi content: ", err);
        res.status(500).send({ message: err.message });
    }
};

exports.show = (req, res) => {
    // Implement logic to find and send a specific Transaksi by ID
    const id = req.params.id;

    Riwayat
        .findById(id)
        .populate('product.data', ['_id', 'namaProduct'])
        .then(async data => {

            res.send(data)
        })
        .catch(err => res.status(404).send({message: err.message})) 
};

exports.update = (req, res) => {
    // Implement logic to update a Transaksi by ID
    const id = req.params.id;

    Riwayat.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
    .then(data => {
        if (!data) {
            console.error("Failed updating Transaksi");
            res.status(500).send({message: "Tidak Dapat Mengupdate Data"})
        }
        res.send({message: "Data berhasil di update"})
    })
    .catch(err => {
        console.error("Failed updating Transaksi: ", err);
        res.status(500).send({message: err.message})
    });
};

exports.delete = (req, res) => {
    // Implement logic to delete a Transaksi by ID
    const id = req.params.id;

    Riwayat.findByIdAndRemove(id)
    .then(data => {
        if (!data) {
            console.error("Failed updating Transaksi");
            res.status(404).send({message: "Tidak Dapat Menghapus Data"})
        }
        res.send({message: "Data berhasil di Hapus"})
    })
    .catch(err => {
        console.error("Failed deleting Transaksi: ", err);
        res.status(500).send({message: err.message})
    });
};
