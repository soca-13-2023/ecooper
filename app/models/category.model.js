module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            namaCategory: {
                type: String,
                require: true
            }
        },
        {
            timestamps: true
        }
    );

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });
    
    const model = mongoose.model("the-category", schema);

    console.log('Abcaa Model', model);
   
    return model;
};
