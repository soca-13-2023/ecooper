/* const Model = require("./base.model")

module.exports = class TransaksiModel extends Model {
    setupSchema() {
        const productSchema = this.mongoose.Schema(
            { name: String }, { discriminatorKey: 'status', _id: false }
        )
    
        this.schema = this.mongoose.Schema(
            {
                type: {
                    type: String,
                    enum: ['beli', 'jual']
                },
                product: productSchema,
                jumlah: Number,
                harga: Number,
                tanggal: Date
            },
            {
                timestamps: true
            }
        );
        this.schema.path('product').discriminator('Available', this.mongoose.Schema({
            data: {
                type: this.mongoose.Schema.Types.ObjectId,
                ref: 'product'
            }
        }, { _id: false }));
        this.schema.path('product').discriminator('Deleted', this.mongoose.Schema({
            data: {
                namaProduct: String
            }
        }, { _id: false }));

        this.schema.pre('save', function(next) {
            console.log('> Hook pre save', this)
            preUpdate(next, this)
        });
        this.schema.pre('findOneAndUpdate', function(next) {
            console.log('> Hook pre update \n', this)
            preUpdate(next, this.getUpdate())
        });
    
        const preUpdate = (next, data) => {
            if (data.product.status == "Deleted") {
                next();
                return;
            } else if (data.product.status != "Available") {
                next(new Error("Discriminator key should be Available or Deleted"));
                return;
            }
    
            let productCount = 0;
            console.log('Check Model Availability', this.product)
            global.models.product.model
                .find({ _id: data.product.data })
                .cursor()
                .eachAsync((doc, i) => {
                    productCount++;
    
        
                    // Calculate the product's stock
                    const stockAkhir = data.type == 'beli' ?
                        doc.stockProduct + data.jumlah :
                        doc.stockProduct - data.jumlah;
        
                    doc.stockProduct = stockAkhir;
                    doc.save();
                })
                //.then(() => productCount > 0 ? success() : failed({ message: 'Tidak ada produk dengan id yang diberikan' }) )
                .then(() => {
                    if (productCount > 0) {
                        next();
                    } else {
                        next(new Error('Tidak ada produk dengan id yang diberikan'));
                    }
                })
                .catch(err => next(err))
        }
    
    
        this.schema.method("toJSON", function(){
            const{__v,_id, ...object} = this.toObject();
            object.id = _id;
    
            return object;
        });
    }

    setupModel() {
        this.model = this.mongoose.model("transaksi", this.schema)
    }
} */

const setupHooks = require('../hooks/riwayat.hooks');

module.exports = (mongoose) => {
    // ===> Create Schemas first
    const productSchema = mongoose.Schema({}, { discriminatorKey: 'status', _id: false })
    const riwayatSchema = mongoose.Schema(
        {
            product: productSchema,
            jumlah: {
                type: Number,
                required: true
            },
            tanggal: {
                type: Date,
                required: true
            }
        }, { discriminatorKey: 'type' }
    );

    const jualSchema = mongoose.Schema({
        hargaJual: {
            type: Number,
            required: true
        }
    });
    const beliSchema = mongoose.Schema({
        hargaBeli: {
            type: Number,
            required: true
        }
    });
/*     const kurangSchema = mongoose.Schema({});
    const tambahSchema = mongoose.Schema({});  
    const dipakaiSchema = mongoose.Schema({
        oleh: productSchema
    }); */

    // Main Schema
    schema = mongoose.Schema(
        riwayatSchema, { timestamps: true }
    );

    // ===> Set-up Discriminator
    schema.discriminator('jual', jualSchema);
    schema.discriminator('beli', beliSchema);
    /* schema.discriminator('kurang', kurangSchema);
    schema.discriminator('tambah', tambahSchema);
    schema.discriminator('dipakai', dipakaiSchema); */

    riwayatSchema.path('product').discriminator('exist', mongoose.Schema({
        data: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'product'
        }
    }, { _id: false }));
    riwayatSchema.path('product').discriminator('deleted', mongoose.Schema({
        data: {
            namaProduct: String
        }
    }, { _id: false }));

    // ===> Set-up Hooks
    setupHooks(schema)

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });

    return mongoose.model("riwayat", schema)
}
