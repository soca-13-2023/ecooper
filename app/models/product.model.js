module.exports = (mongoose) => {
    const setupHook = require('../hooks/product.hooks');
    
    // ===> Create Schemas first
    // Main Schema
    schema = mongoose.Schema({
        dijual: {
            type: Boolean,
            required: true,
            default: true
        },
        namaProduct: {
            type: String,
            required: true
        },
        stockProduct: {
            type: Number,
            required: true
        },
        hargaJual: {
            type: Number,
            required: true
        },
        the-category: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'the-category'
        }
    },
    {
        timestamps: true
    });

    // ===> Set-up Hooks
    setupHook(schema);

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });

    // Override Product's find function to suit our need
    /* model._find = model.find;
    model.find = function(predicate, thisArgs) {
        const query = model._find(predicate, thisArgs)
        query._exec = query.exec

        query.populate('induk.product.data', ['namaProduct', 'stockProduct'])
        query.exec = async () => {
            return findExec(model, await query._exec())
        }

        return query
    }; */

    return mongoose.model("product", schema);
}
