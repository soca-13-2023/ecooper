const removeAllReference = async (idProduct, next) => {
    const Riwayat = global.database.riwayat;

    // await Riwayat
    //     .find({ 'product.data': idProduct })
    //     .populate('product.data', 'namaProduct')
    //     .cursor()
    //     .eachAsync((transaksi, i) => {
    //         if (transaksi.product.status != 'exist')
    //             return;

    //         transaksi.product = {
    //             status: 'deleted',
    //             /* data: {
    //                 namaProduct: transaksi.product._doc.data.namaProduct
    //             } */
    //         }
            
    //         transaksi.save();
    //     });

    await Riwayat.findByIdAndUpdate(
        { 'product.data': idProduct },
        {
            $set: {
                'product.status': 'deleted',
                'product.data': {
                    namaProduct: 'abc'
                }
            },
            $unset: [
                'product.data'
            ]
        },
        { overwriteDiscriminatorKey: true, new: true }
    );

    // await Riwayat.updateMany(
    //     { 'product.data': idProduct },
    //     {
    //         'data': {
    //             'namaProduct': 'Abc'
    //         }
    //     },
    //     { overwriteDiscriminatorKey: true, new: true }
    // );

    next('Abatasajahaho');
}

// Check if there is matching product in Product document
const validateData = async (next, data) => {
    const the-category = await global.database.the-category.findById(data.the-category).exec();
    console.log('Validate Data', the-category);
    if (!the-category) {
        console.log('Ok!');
        next('Tidak ada Category dengan id yang diberikan');
    }
    else
        next();
}

module.exports = (schema) => {
    // schema.pre('findOneAndRemove', function (next) { removeAllReference(this.getFilter()._id, next) });
    // schema.pre('remove', function (next) { removeAllReference(this, next) });
    schema.pre('save', function(next) {
        console.log('> Hook pre save', this)
        validateData(next, this)
    });
    schema.pre('findOneAndUpdate', function(next) {
        console.log('> Hook pre update \n', this)
        vaildateData(next, this.getUpdate())
    });
}
