/* const Hook = require("./base.hooks");

module.exports = class TransaksiHook extends Hook {
    // Transaksi Product
    #product;

    setupHooks() {
        console.log('Called')
        // Update Product's stock automatically when there is a new data/update in Transaksi document.
        this.schema.pre('save', function(next) {
            console.log('> Hook pre save', this)
            preUpdate(next, this)
        });
        this.schema.pre('findOneAndUpdate', function(next) {
            console.log('> Hook pre update \n', this)
            preUpdate(next, this.getUpdate())
        });
    
        const preUpdate = (next, data) => {
            if (data.product.status == "Deleted") {
                next();
                return;
            } else if (data.product.status != "Available") {
                next(new Error("Discriminator key should be Available or Deleted"));
                return;
            }
    
            let productCount = 0;
            console.log('Check Model Availability', this.product)
            this.product
                .find({ _id: data.product.data })
                .cursor()
                .eachAsync((doc, i) => {
                    productCount++;
    
        
                    // Calculate the product's stock
                    const stockAkhir = data.type == 'beli' ?
                        doc.stockProduct + data.jumlah :
                        doc.stockProduct - data.jumlah;
        
                    doc.stockProduct = stockAkhir;
                    doc.save();
                })
                //.then(() => productCount > 0 ? success() : failed({ message: 'Tidak ada produk dengan id yang diberikan' }) )
                .then(() => {
                    if (productCount > 0) {
                        next();
                    } else {
                        next(new Error('Tidak ada produk dengan id yang diberikan'));
                    }
                })
                .catch(err => next(err))
        }
    }

    finishSetup(product) {
        this.#product = product;
    }
} */

// Check if there is matching product in Product document
const preSave = (next, data) => {
    if (data.product.status == "deleted") {
        next();
        return;
    } else if (data.product.status != "exist") {
        next(new Error("Discriminator key should be 'exist' or 'deleted'"));
        return;
    }

    let productCount = 0;
    global.database.product
        .find({ _id: data.product._doc.data })
        .cursor()
        .eachAsync((doc, i) => {
            productCount++;

            // Calculate the product's stock
            const stockAkhir = data.type == 'beli' ?
                doc.stockProduct + data.jumlah :
                doc.stockProduct - data.jumlah;

            doc.stockProduct = stockAkhir;
            doc.save();
        })
        //.then(() => productCount > 0 ? success() : failed({ message: 'Tidak ada produk dengan id yang diberikan' }) )
        .then(() => {
            if (productCount > 0) {
                next();
            } else {
                next(new Error('Tidak ada produk dengan id yang diberikan'));
            }
        })
        .catch(err => next(err))
}
 
module.exports = (schema) => {
    schema.pre('save', function(next) {
        console.log('> Hook pre save', this)
        preSave(next, this)
    });
    schema.pre('findOneAndUpdate', function(next) {
        console.log('> Hook pre update \n', this)
        preSave(next, this.getUpdate())
    });
}