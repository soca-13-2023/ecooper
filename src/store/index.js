import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    msg: 'Guest',
    nama: 'Guest'
  },
  mutations: {
    updateMsg(state, newMsg) {
      state.msg = newMsg;
    },
    updateNama(state, newNama) {
      state.nama = newNama
      sessionStorage.setItem("username", newNama)
    }
  },
});
