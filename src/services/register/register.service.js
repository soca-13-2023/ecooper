import axios from "../../config/axios";

export async function signup(formData) {
    return axios.post('/account/register', formData)
    .then((res) => {
        console.log('Berhasil')
        return res.data
    })
    .catch((error) => {
        console.error("Failed doing Register", error)
        const data = {
            status: error.response !== undefined && error.response.data.status !== undefined ?
                error.response.data.status :
                'failed',
            response: error.response !== undefined && error.response.data.message !== undefined ?
                error.response.data.message :
                error.message
        }
        return data
    })
}