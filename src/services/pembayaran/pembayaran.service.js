import axios from "../../config/axios";

export async function checkout(idProduct, jumlah) {
    return axios.post('/pembayaran/checkout', { idProduct, jumlah })
        .then((res) => {
            return res.data
        })
        .catch((error) => {
            console.error("Failed do Checkout", error)
            const data = {
                status: error.response !== undefined && error.response.data.status !== undefined ?
                    error.response.data.status :
                    'failed',
                response: error.response !== undefined && error.response.data.message !== undefined ?
                    error.response.data.message :
                    error.message
            }
            return data
        })
}

export async function getPembayaran() {
    return axios.get('/pembayaran/getPembayaranThisUser')
        .then((res) => {
            return res.data
        })
        .catch((error) => {
            console.error("Failed getting Pembayaran", error)
            const data = {
                status: error.response !== undefined && error.response.data.status !== undefined ?
                    error.response.data.status :
                    'failed',
                response: error.response !== undefined && error.response.data.message !== undefined ?
                    error.response.data.message :
                    error.message
            }
            return data
        })
}