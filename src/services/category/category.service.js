import axios from "../../config/axios";

export async function getCategory() {
    return axios.get('/category')
    .then((res) => {
        return res.data
    })
    .catch((error) => {
        const data = { status: false, response: error.response }
        return data
    })
}