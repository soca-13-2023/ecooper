import axios from "../../config/axios";

export async function getProfileDetail() {
    return axios.get('/account');
}

export async function updateProfile() {
    return axios.post('/pembayaran/checkout', { idProduct, jumlah })
        .then((res) => {
            return res.data
        })
        .catch((error) => {
            console.error("Failed do Checkout", error)
            const data = {
                status: error.response !== undefined && error.response.data.status !== undefined ?
                    error.response.data.status :
                    'failed',
                response: error.response !== undefined && error.response.data.message !== undefined ?
                    error.response.data.message :
                    error.message
            }
            return data
        })
}