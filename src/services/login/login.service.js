import axios from "../../config/axios";

export async function signin(formData) {
    return axios.post('/account/login', formData)
    .then((res) => {
        return res.data
    })
    .catch((error) => {
        console.error("Failed signing in", error)
        const data = {
            status: error.response !== undefined && error.response.data.status !== undefined ?
                error.response.data.status :
                'failed',
            response: error.response !== undefined && error.response.data.message !== undefined ?
                error.response.data.message :
                error.message
        }
        return data
    })
}