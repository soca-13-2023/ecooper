import axios from "../../config/axios";

export async function getProduct() {
    return axios.get('/product')
    .then((res) => {
        return res.data
    })
    .catch((error) => {
        const data = { status: false, response: error.response }
        return data
    })
}

export async function getProductByCategory(id) {
    return axios.get('/product/getbycategory/' + id)
    .then((res) => {
        return res.data
    })
    .catch((error) => {
        const data = { status: false, response: error.response }
        return data
    })
}

export async function getDetailProduct(id) {
    return axios.get('/product/' + id)
    .then((res) => {
        return res.data
    })
    .catch((error) => {
        const data = { status: false, response: error.response }
        return data
    })
}