import axios from "../../config/axios";

export async function signout() {
    return axios.post('/account/logout')
    .then((res) => {
        return res.data
    })
    .catch((error) => {
        console.error("Failed signing out", error)
        const data = {
            status: error.response !== undefined && error.response.data.status !== undefined ?
                error.response.data.status :
                'failed',
            response: error.response !== undefined && error.response.data.message !== undefined ?
                error.response.data.message :
                error.message
        }
        return data
    })
}