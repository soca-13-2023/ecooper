import axios from "axios"
import { base_url } from "./base_url"

axios.defaults.baseURL = base_url;
axios.defaults.withCredentials = true;
export default axios;