import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/HomeView.vue')
  },
  {
    path: '/category',
    name: 'category',
    component: () => import('@/views/CategoryView.vue')
  },
  {
    path: '/product',
    name: 'products',
    component: () => import('@/views/product/ProductView.vue')
  },
  {
    path: '/product/detail/:id',
    name: 'products-detail',
    component: () => import('@/views/DetailProductView.vue')
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('@/views/ContactView.vue')
  },
  {
    path: '/pembeli-login',
    name: 'pembeli-login',
    component: () => import('@/views/login/PembeliView.vue')
  },
  {
    path: '/pembeli-register',
    name: 'pembeli-register',
    component: () => import('@/views/register/PembeliView.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('@/views/profile/ProfileView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
